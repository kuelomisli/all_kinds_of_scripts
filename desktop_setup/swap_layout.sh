#!/usr/bin/bash

KEYBOARD=$(xinput | perl -ne '/translated set/i && s/.*id=(\d{1,2}).*/$1/gi && print ')
MASTER_KEYBOARD=$(xinput | perl -ne '/Virtual core keyboard/i && s/.*id=(\d{1,2}).*/$1/gi && print ')
cd /tmp
xkbcomp -i $KEYBOARD $DISPLAY -o layout.tmp
layout=$(sed -rn '/xkb_symbols/ s/.*[_+](us|de)[_+].*/\1/p' layout.tmp)
rm layout.tmp

#echo layout is $layout
#echo display is $DISPLAY
#echo KEYBOARD is $KEYBOARD

HOST=$(hostname)

case $HOST in
    'monolith')

    case $layout in
        'us')
            xkbcomp -i $KEYBOARD /opt/dev/all_kinds_of_scripts/desktop_setup/keyboard_de.xkb $DISPLAY
            ;;
        'de')
            xkbcomp -i $KEYBOARD /opt/dev/all_kinds_of_scripts/desktop_setup/keyboard.xkb $DISPLAY
            ;;
    esac
    ;;
    'workpc')
    xkbcomp -i $MASTER_KEYBOARD $DISPLAY -o layout.tmp
    layout=$(sed -rn '/xkb_symbols/ s/.*[_+](us|de)[_+].*/\1/p' layout.tmp)
    rm layout.tmp
    case $layout in
        'us')
            #xkbcomp -i $MASTER_KEYBOARD /opt/dev/all_kinds_of_scripts/desktop_setup/dell_de.xkb $DISPLAY
            setxkbmap -layout de ; xmodmap ~/.Xmodmap
            ;;
        'de')
            #xkbcomp -i $MASTER_KEYBOARD /opt/dev/all_kinds_of_scripts/desktop_setup/dell.xkb $DISPLAY
            setxkbmap -layout us ; xmodmap ~/.Xmodmap
            ;;
    esac
    ;;
esac
