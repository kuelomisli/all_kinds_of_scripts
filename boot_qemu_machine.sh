#!/bin/bash

qemu-system-x86_64 \
-machine accel=kvm \
-cpu host \
-name WindowsXP \
-drive file=./WindowsXP.img,if=ide,cache=writeback \
-boot c \
-m 1G \
-enable-kvm \
-usb \
-device usb-host,hostbus=1,hostaddr=5
#-cdrom winxp.iso \

#058f:6387
#-full-screen 

#-smp 1,sockets=1,cores=1,threads=1 \
#-nodefaults \
#-rtc base=localtime \
#-vnc 127.0.0.1:1 \
#-device e1000,netdev=net0 \
#-netdev user,id=net0

#-monitor stdio \
#-boot order=c \
#-vga qxl \
#-device hda-duplex,id=sound0-codec0,bus=sound0.0,cad=0 \
#-device virtio-balloon-pci,id=balloon0,bus=pci.0,addr=0x6 \
#-device usb-tablet \
#-cpu core2duo \
