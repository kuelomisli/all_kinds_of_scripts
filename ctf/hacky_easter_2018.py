#!/bin/python

import re
import base64
import struct
import json
import itertools

#tony = "gn tn-gha87be4e"
tony = "gn tn-gha87be4e"
recipe = "c2FsdA=="
alphabet = "abcdefghijklmnopqrstuvwxyz0123456789"

with open('./codexofhammurabi.txt','r') as f:
    hammurabi = f.read()

with open('./genesis11.txt','r') as f:
    genesis11 = f.read()

with open('./babylon.txt','rb') as f:
    babylon = f.read()
    babylon = babylon.decode('utf8')

babylon = babylon[:-1]

def caesar(offset,instring):
    decoded = "".join( [chr(ord(x)+offset) for x in instring] )
    return decoded

def caesar_cyclical(offset,instring,only_lower):

    d = []
    for x in instring:
        offset = offset%127
        if (ord(x)+offset >= 127):
            d.append(chr(32 + (ord(x) + offset) - 127))
        else:
            d.append(chr(ord(x) + offset))
    return ''.join(d)

def vigenere(plaintext, key):
    nums = [ord(x)-31 for x in key]
    o = ""
    for x,n in zip(plaintext,itertools.cycle(nums)):
        o += caesar_cyclical(n,x,0)
    return o
#print(caesar_cyclical(3,"-",0))
#print(vigenere(tony,"tonythepony"))

#for x in range(0,len(babylon)-4,4):
a = []
for x in range(0,95):
#    print(caesar_cyclical(4,babylon[x]),end='')
#    print(caesar_cyclical(4,babylon[x+1]),end='')
#    print(caesar_cyclical(28,babylon[x+2]),end='')
    #print(caesar_cyclical(355,babylon[x+3]),end='')
    #a.append((caesar_cyclical(x,tony,0).lower()))
    continue
    #print(caesar(x,babylon))
    #print(chr(x+ord("0")))

b = []
for caesar in a:
    for i,x in enumerate(caesar):
        b.append(caesar[i:]+caesar[:i])
#print(b)
#with open("./tonyperms.json","wb") as f:
#    string = json.dumps(b)
#    f.write(string.encode('utf8'))
alphabetic = re.compile(r'[0-9]*([a-zA-Z]+)[0-9]*')
numeric = re.compile(r'[a-zA-Z]*([0-9]+)[a-zA-Z]*')

#for x,y in zip("b2ls","salt"):
#    print(ord(x)-ord(y))

#print(len(babylon))
#print(babylon[3])
#print(babylon[-4])
#print(babylon[-28])
#print(babylon[-355])

print(len(genesis11))
print(genesis11[3])
print(genesis11[7])
print(genesis11[27])
print(genesis11[354])
res = alphabetic.findall(babylon)


print(len(hammurabi))

b=[]
#
#for i in range(0,100):
#    try:
#        print("tried")
#        b = base64.b32decode(babylon)
#        print(b)
#        break
#    except:
#        babylon = ''.join([babylon,'='])
#    #else:
#    #    print("else run")
#    #finally:
#    #    print("finally run")
#    

count = 0
histogramm = dict.fromkeys(list(alphabet),0)
for x in babylon:
    histogramm[x] += 1

print(sorted(histogramm,key=lambda x: histogramm[x]))
print(histogramm)
print(babylon[935])

print("4 appears so many times in babylon: ", count)

print("TONY")
print(len(tony))
for x in tony:
    print(ord(x),x)
#byts = struct.pack('15c',*tony)

tony = "gn tn-gha87be4e"
corks = [103,110,32,116,110,22,103,104,97,56,55,98,101,52,101]
print(''.join([chr(x) for x in corks]))

#bstring = "BUDRCUAhCFzlhum3j6AJpGdUwoszcH0pMVHNLr+NpiHPhej/GsepCQ=="
#b = base64.b85decode(babylon)
#print((b))
#with open('./bytes.b','wb') as f:
#    f.write(b)

#res = numeric.findall(babylon)
#print(res[3])
#print(res[7])
#print(res[35])
#print(res[390])

#ij ijhfs
#tony log:
# tried offset+,forwards and backwards
# offset - goes below space in ascii
# ..unless.. cyclical? but then Upper Case could come in..


