#!/bin/bash

#copy current .Xresources to .Xresources.inv, swap with .inv, xrdb -merge

CDIR=/opt/dev/scripts_and_configs/configs

INVERTED=`ls -l /home/$USER/.Xresources | grep -o .Xresources.inv$`
if [[ $INVERTED != '' ]];
then
    rm /home/$USER/.Xresources 
    ln -s $CDIR/.Xresources /home/$USER/.Xresources
else
    rm /home/$USER/.Xresources
    ln -s $CDIR/.Xresources.inv /home/$USER/.Xresources
fi

xrdb -merge /home/$USER/.Xresources

