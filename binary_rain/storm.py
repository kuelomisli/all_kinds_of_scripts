#!/usr/bin/env python

# This does funny things with ANSI escape sequences

import sys
import time
import math
import numpy as np
import pyaudio
import random 

base_cell = '...'

volume = 0.1     # range [0.0, 1.0]
fs = 44100       # sampling rate, Hz, must be integer
duration = 0.1  # in seconds, may be float
f = 110.0        # sine frequency, Hz, may be float

sleep_time = 0.15

    #sleep_time = duration + sleep_time/10

p = pyaudio.PyAudio()
# generate samples, note conversion to float32 array
samples = (np.sin(2*np.pi*np.arange(fs*duration)*f/fs)).astype(np.float32)

# for paFloat32 sample values must be in range [-1.0, 1.0]
stream = p.open(format=pyaudio.paFloat32,
                channels=1,
                rate=fs,
                output=True)

# play. May repeat with different volume values (if done interactively)

delay_basis = 0.5
field_width = 122
field_length = 28
#field_width = 40
#field_length = 10

left_upper = '\x1b[H' 
start_line = 2
go_home = ''.join(['\x1b[',str(start_line+1),';1H'])
row = base_cell*int(field_width/len(base_cell))
field = [ ''.join([row,'\n']) for i in range(field_length+1)]
field = ''.join(field)

#sys.stdout.write(field)
#sys.stdout.flush()
#
#one = '\2' + '\n'*random.randint(0,field_length)+' '*random.randint(0,field_width)+'1'
#sys.stdout.write(one)
#sys.stdout.flush()

# Set Cursor to n;m
#sys.stdout.write('\033['+'J')
sys.stdout.write(left_upper)
sys.stdout.write('\x1b[J')
#sys.stdout.write('\0xa')
#sys.stdout.flush()
sys.stdout.write(go_home)
sys.stdout.flush()
#sys.stdout.write('\033[1;1f')
#sys.stdout.flush()
# Clear from Cursor to End of Screen
sys.stdout.write('\033['+'J')
sys.stdout.flush()
sys.stdout.write(field)
sys.stdout.flush()
for i in range(1,100000):
    if sleep_time < duration:
        duration = duration - sleep_time/10
    sleep_time =  1/math.sqrt(i)
    home =  '\x1b[H' 
    # fix: don't overwrite previous ones with zeroes
    one = '\x1b[H' + '\n'*random.randint(start_line,field_length)+base_cell*random.randint(1,int(field_width/len(base_cell)-1))+'1'
    time.sleep(sleep_time)
    stream.start_stream()
    stream.write(volume*samples)
    stream.stop_stream()
    sys.stdout.write(one)
    sys.stdout.flush()
    #time.sleep(1)
#    delay = delay_basis-math.sqrt(delay_basis*10/20*i)/10
#zeroes = '0 '*int(field_width/2) 
#sys.stdout.write('\x0d\x0a')
#sys.stdout.write('\033['+str(field_length+1)+';'+str(field_width)+'H')

#Set End Position for prompt after exit:
sys.stdout.write('\033['+str(field_length+3)+';'+'1'+'f')
sys.stdout.write('\033['+'J')
sys.stdout.flush()
sys.stdout.write('\x1b[0;0H')
#print(home)
sys.stdout.flush()
sys.stdout.write('\033['+'J')
sys.stdout.flush()


stream.stop_stream()
stream.close()

p.terminate()
