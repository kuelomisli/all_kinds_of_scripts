#!/bin/sh
TRACKPAD=$(xinput | sed -nr '/Synaptics/ s/.*([0-9]{2}).*/ \1 /p')

STATE=$(xinput --list $TRACKPAD | sed -nr 's/.*?[tT]his device is (.*?).*/\1/p')

echo "Trackpad $TRACKPAD is $STATE"

if [[ "$STATE" == *"disabled"* ]]; then
    echo "it's disabled"
    xinput --enable $TRACKPAD
fi

if [[ "$STATE" == "" ]]; then
    echo "it's enabled"
    xinput --disable $TRACKPAD
fi
#xinput --disable $TRACKPAD
