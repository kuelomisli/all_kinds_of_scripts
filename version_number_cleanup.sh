#!/bin/bash 

# This script cleans the version numbers from words, e.g.
# linux-headers-4.16-rc3 would become linux-headers.
# Just give it one argument and it will output the result.

echo $1 | sed -r 's/([\w-]*)-[0-9]+[.:]*.*/\1/'

# example usage:
# lots of package names separated by whitespaces in packages.txt:
# cat ./packages.txt | xargs -I {} -d\  echo {} | sed -nr '/.+/ p' | xargs -I {} ./version_number_cleanup.sh {} | tr '\n' ' ' 
