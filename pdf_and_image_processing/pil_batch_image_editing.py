#!/usr/bin/python

import os,sys
from PIL import Image,ImageEnhance,ImageOps

#with open('./0474_001.pdf','rb') as f:
#    raw = f.read()


#im = Image.frombytes(0, len(raw),raw)
for infile in sys.argv[1:]:
    f, e = os.path.splitext(infile)
    outfile = f + '-grey-poster' + '.tif'
    try:
        im = Image.open(infile)
        enhancer = ImageEnhance.Color(im)
        enhancer.enhance(0.0).save(outfile)
        #operator = ImageOps.posterize(im,4)
        #operator = ImageOps.autocontrast(im, 30)
        #operator.save(outfile)

    except:
        print("Error encountered!")


#im = Image.open('./test.tiff')

#im.show()
